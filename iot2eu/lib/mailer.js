var nodemailer = require('nodemailer');

// Its not a good idea to provide your credentials like this, they should come from an external source. This is only for the demo.
var EMAIL_ACCOUNT_USER = 'postmaster@mg.faks-up.org';
var EMAIL_ACCOUNT_PASSWORD = '57045bc3e2d6bcc4b6977613494c3341';
var EMAIL_FROM = "info@iot2.eu";
var YOUR_NAME = 'IOT2EU';

//reusable transport, look at the docs to see other service/protocol options
//SMTP Transport is default one
var smtpTransport = nodemailer.createTransport({
    service: "Mailgun",
    auth: {
        user: EMAIL_ACCOUNT_USER,
        pass: EMAIL_ACCOUNT_PASSWORD
    }
});

// Public method that actually sends the email
exports.sendMail = function( toAddress, data){
    var mailOptions = {
        // NOTE: the fromAdress can actually be different than the email address you're sending it from. Which is good and bad I suppose. Use it wisely.
        from: YOUR_NAME + ' <' + EMAIL_ACCOUNT_USER + '>',
        to: toAddress,
        replyTo: EMAIL_FROM,
        subject: "Prijava za IoT radionicu",
        html:   "<h3>Nova prijava</h3><h4>" + data.name + " " + data.prezime + "</h4><h4>" + data.email + " " + data.ustanova + "</h4> <p>" + data.message + "</p>"
    };

    // send the email!
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log('[ERROR] Message NOT sent: ', error);
        }
        else {
            console.log('[INFO] Message Sent: ' + response.message);
            var mailOptions = {
                // NOTE: the fromAdress can actually be different than the email address you're sending it from. Which is good and bad I suppose. Use it wisely.
                from: YOUR_NAME + ' <' + EMAIL_ACCOUNT_USER + '>',
                to: data.email,
                replyTo: EMAIL_FROM,
                subject: "Prijava na IoT2EU",
                html:   "<p> Poštovani,<br><br> Zahvaljujemo na prijavi.<br> Pratite promjene na <a href='htpp://iot2.eu'>http://iot2.eu</a>!<br>Lijep pozdrav, <br><br> IoT2EU Organizacijski odbor</p>"
            };
            // send the email!
            smtpTransport.sendMail(mailOptions, function(error, response){
                if(error){
                    console.log('[ERROR] Message NOT sent: ', error);
                }
                else {
                    console.log('[INFO] cONFIRMATION Sent: ' + response.message);
                }
            });
        }
    });
};