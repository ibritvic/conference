/**
 * Created by ibritvic on 1/24/15.
 */
module.exports = {
    development: {
        db: 'mongodb://localhost/test',
        port: 3011
    },
    production: {
        db: 'mongodb://localhost/rpi',
        port: 3011
    }
};
