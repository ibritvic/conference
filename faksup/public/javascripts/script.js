function initialize() {
  var myLatlng = new google.maps.LatLng(45.8086682,15.9624029);
  var mapOptions = {
    zoom: 18,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      animation: google.maps.Animation.DROP,

      title: 'VsiTe - Visoka škola za informacijske tehnologije',
      icon: 'images/v.png'
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
       


$(document).ready(function() {

	// hide #back-top first
    $("#up").hide();
    $("#flogo").hide();
    $(".menubar").show();


    // fade in #back-top
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#up').fadeIn();
                $('#flogo').fadeIn();
                
            } else {
                $('#up').fadeOut();
                $('#flogo').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('#up').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
	});

	$(function() {
		$("#dialog").dialog({
			autoOpen: false,
			//modal: true,
			resizable: false,
			hide: {
        		effect: "fade",
        		duration: 200
      		},
      		show: {
      			effect:"fade",
      			duration: 500
      		}
		});


	$("#signup").on("click", function() {
		$("#dialog").dialog("open");
		});
	});
	
	// Validating Form Fields.....
	$("#prijava").submit(function(e) {
		e.preventDefault();
        return; //do nothing
		var name = $("#name").val();
		var lastname =$("#lastname").val();
		var email = $("#email").val();
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if (email === '' || name === '' || lastname == '') {
			$("#error_validate").fadeIn(2000,function() {
  				$("#error_validate").fadeOut(2000);
			});
		} else if (!(email).match(emailReg)) {
			$("#error_mail").fadeIn(2000,function() {
  				$("#error_mail").fadeOut(2000);
			});
		} else {
			$.post( "/prijava", $( "#prijava" ).serialize())
				.done(function() {
                    $("#msg").fadeIn(1000,function(){
                        setTimeout(function(){
                            $("#dialog").dialog("close")
                            $("#msg").hide();
                            var fm=document.getElementById("prijava");
                            fm.reset();
                        }, 3000);
                    });
				})
				.fail(function(err) {
                    if (err.status == 520)
                        $("#error_duplicate").fadeIn(2000,function() { //trebalo bi u funkciju
                            $("#error_duplicate").fadeOut(3000);
                        });
                    else
                        $("#error_generic").fadeIn(2000,function() {
                            $("#error_generic").fadeOut(3000);
                        });

				});
		}
	});
});

