var express = require('express');
var router = express.Router();
var participant = require('../controllers/participant');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'VsiTe faks-up' });
});

/* GET pregled. */
router.get('/pregled', function(req, res) {
    res.render('pregled', { title: 'VsiTe faks-up pregled' });
});
/* GET galerija */
router.get('/galerija', function(req, res) {
    res.render('galerija', { title: 'VsiTe faks-up galerija' });
});

/* POST prijava. */
router.post('/prijava', participant.create);

module.exports = router;
