/**
 * Created by ibritvic on 1/13/15.
 */

var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

var participantSchema = mongoose.Schema({
    date: { type: Date, default: Date.now },
    name: String,
    lastname: String,
    email: {type:String , unique : true}
});

//REgister model
mongoose.model('Participant', participantSchema);