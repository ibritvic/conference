/**
 * Created by ibritvic on 1/24/15.
 */
module.exports = {
    development: {
        db: 'mongodb://localhost/test',
        port: 3007
    },
    production: {
        db: 'mongodb://localhost/conference',
        port: 3007
    }
};
