var nodemailer = require('nodemailer');

// Its not a good idea to provide your credentials like this, they should come from an external source. This is only for the demo.
var EMAIL_ACCOUNT_USER = 'postmaster@mg.faks-up.org';
var EMAIL_ACCOUNT_PASSWORD = '57045bc3e2d6bcc4b6977613494c3341';
var EMAIL_FROM = "info@faks-up.org";
var YOUR_NAME = 'Faks-up 2015 Konferencija';

//reusable transport, look at the docs to see other service/protocol options
//SMTP Transport is default one
var smtpTransport = nodemailer.createTransport({
    service: "Mailgun",
    auth: {
        user: EMAIL_ACCOUNT_USER,
        pass: EMAIL_ACCOUNT_PASSWORD
    }
});

// Public method that actually sends the email
exports.sendMail = function( toAddress, ime){
    var mailOptions = {
        // NOTE: the fromAdress can actually be different than the email address you're sending it from. Which is good and bad I suppose. Use it wisely.
        from: YOUR_NAME + ' <' + EMAIL_ACCOUNT_USER + '>',
        to: toAddress,
        replyTo: EMAIL_FROM,
        subject: "Potvrda prijave",
        html:   "Poštovani " + ime +
                ", <br><br>Uspješno ste se prijavili na konferenciju! <br>" +
                "Vidimo se 19. veljače 2015 u Klaićevoj 7.<br><br>" +
                "Faks-up 2015 organizacijski odbor"
    };

    // send the email!
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log('[ERROR] Message NOT sent: ', error);
        }
        else {
            console.log('[INFO] Message Sent: ' + response.message);
        }
    });
};