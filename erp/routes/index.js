var express = require('express');
var router = express.Router();
var mailer = require('../lib/mailer');
//var participant = require('../controllers/participant');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'ERP Implementacije' });
});
//ivbritvic@gmail.com

/* POST prijava. */
router.post('/mail/contact', function(req, res){
  mailer.sendMail('anikitovic@vsite.hr', req.body);
  res.status(200).end();
});

module.exports = router;
