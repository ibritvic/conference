var nodemailer = require('nodemailer');

// Its not a good idea to provide your credentials like this, they should come from an external source. This is only for the demo.
var EMAIL_ACCOUNT_USER = 'postmaster@mg.faks-up.org';
var EMAIL_ACCOUNT_PASSWORD = '57045bc3e2d6bcc4b6977613494c3341';
var EMAIL_FROM = "info@iot2.eu";
var YOUR_NAME = 'ERP Implementacija';

//reusable transport, look at the docs to see other service/protocol options
//SMTP Transport is default one
var smtpTransport = nodemailer.createTransport({
    service: "Mailgun",
    auth: {
        user: EMAIL_ACCOUNT_USER,
        pass: EMAIL_ACCOUNT_PASSWORD
    }
});


// Public method that actually sends the email
exports.sendMail = function( toAddress, data){
    var mailOptions = {
        // NOTE: the fromAdress can actually be different than the email address you're sending it from. Which is good and bad I suppose. Use it wisely.
        from: YOUR_NAME + ' <' + EMAIL_ACCOUNT_USER + '>',
        to: toAddress,
        replyTo: EMAIL_FROM,
        subject: "Poruka preko weba",
        html:   "<h3>Prijava</h3><h4>" + data.name + "</h4><h4>" + data.email + "</h4> <p>" + data.message + "</p>"
    };

    // send the email!
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log('[ERROR] Message NOT sent: ', error);
        }
        else {
            console.log('[INFO] Message Sent: ' + response.message);
            var mailOptions = {
                // NOTE: the fromAdress can actually be different than the email address you're sending it from. Which is good and bad I suppose. Use it wisely.
                from: YOUR_NAME + ' <' + EMAIL_ACCOUNT_USER + '>',
                to: data.email,
                replyTo: EMAIL_FROM,
                subject: "Prijava na erp Implementacija",
                html:   "<p> Poštovani,<br><br> Zahvaljujemo na prijavi.<br> Pratite promjene na <a href='http://erp-implementacija.com'>http://erp-implementacija.com</a>!<br>Lijep pozdrav, <br><br> Organizacijski odbor</p>"
            };
            // send the email!
            smtpTransport.sendMail(mailOptions, function(error, response){
                if(error){
                    console.log('[ERROR] Message NOT sent: ', error);
                }
                else {
                    console.log('[INFO] cONFIRMATION Sent: ' + response.message);
                }
            });
        }
    });
};